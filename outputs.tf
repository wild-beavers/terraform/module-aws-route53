######
## Hosted zone
######

output "delegation_set_ids" {
  value = length(local.delegation_sets) != 0 ? { for k, v in aws_route53_delegation_set.this : k => v.id } : null
}

output "delegation_set_arns" {
  value = length(local.delegation_sets) != 0 ? { for k, v in aws_route53_delegation_set.this : k => v.arn } : null
}

output "delegation_set_name_servers" {
  value = length(local.delegation_sets) != 0 ? { for k, v in aws_route53_delegation_set.this : k => v.name_servers } : null
}

output "public_zone_arns" {
  value = length(local.public_zones) != 0 ? { for k, v in aws_route53_zone.public : k => v.arn } : null
}

output "public_zone_ids" {
  value = length(local.public_zones) != 0 ? { for k, v in aws_route53_zone.public : k => v.zone_id } : null
}

output "public_zone_domain_names" {
  value = length(local.public_zones) != 0 ? { for k, v in aws_route53_zone.public : k => v.name } : null
}

output "public_zone_name_servers" {
  value = length(local.public_zones) != 0 ? { for k, v in aws_route53_zone.public : k => v.name_servers } : null
}

output "private_zone_arns" {
  value = length(local.private_zones) != 0 ? { for k, v in aws_route53_zone.private : k => v.arn } : null
}

output "private_zone_ids" {
  value = length(local.private_zones) != 0 ? { for k, v in aws_route53_zone.private : k => v.zone_id } : null
}

output "private_zone_domain_names" {
  value = length(local.private_zones) != 0 ? { for k, v in aws_route53_zone.private : k => v.name } : null
}

output "private_zone_name_servers" {
  value = length(local.private_zones) != 0 ? { for k, v in aws_route53_zone.private : k => v.name_servers } : null
}

######
## Resolver endpoints inbound
######

output "resolver_inbound_security_group_ids" {
  value = [for k in aws_security_group.this_inbound : k.id]
}

output "resolver_inbound_ids" {
  value = [for k in aws_route53_resolver_endpoint.this_inbound : k.id]
}

output "resolver_inbound_arns" {
  value = [for k in aws_route53_resolver_endpoint.this_inbound : k.arn]
}

output "resolver_inbound_host_vpc_ids" {
  value = [for k in aws_route53_resolver_endpoint.this_inbound : k.host_vpc_id]
}

#####
# Resolver endpoints outbound
#####

output "resolver_outbound_security_group_id" {
  value = [for k in aws_security_group.this_outbound : k.id]
}

output "resolver_outbound_ids" {
  value = [for k in aws_route53_resolver_endpoint.this_outbound : k.id]
}

output "resolver_outbound_arns" {
  value = [for k in aws_route53_resolver_endpoint.this_outbound : k.arn]
}

output "resolver_outbound_host_vpc_ids" {
  value = [for k in aws_route53_resolver_endpoint.this_outbound : k.host_vpc_id]
}

#####
# Forward rules
#####

output "rule_forward_ids" {
  value = [for k in aws_route53_resolver_rule.this_forward : k.id]
}

output "rule_forward_arns" {
  value = [for k in aws_route53_resolver_rule.this_forward : k.arn]
}

output "rule_forward_owner_ids" {
  value = [for k in aws_route53_resolver_rule.this_forward : k.owner_id]
}

output "rule_forward_share_statuses" {
  value = [for k in aws_route53_resolver_rule.this_forward : k.share_status]
}

output "rule_association_forward_id" {
  value = [for k in aws_route53_resolver_rule_association.this_forward : k.id]
}

#####
# Resource share
#####

output "rule_forward_share_ids" {
  value = [for k in aws_ram_resource_share.this_forward : k.id]
}

output "rule_forward_share_arns" {
  value = [for k in aws_ram_resource_share.this_forward : k.arn]
}

output "resource_association_forward_id" {
  value = [for k in aws_ram_resource_association.this_forward : k.id]
}

output "principal_association_forward_id" {
  value = [for k in aws_ram_principal_association.this_forward : k.id]
}

#####
# Records
#####

output "record_names" {
  value = length(local.records) != 0 || length(local.alias_records) != 0 ? merge(
    { for k, v in aws_route53_record.this : k => v.name },
    { for k, v in aws_route53_record.this_alias : k => v.name }
  ) : null
}

output "record_fqdns" {
  value = length(local.records) != 0 || length(local.alias_records) != 0 ? merge(
    { for k, v in aws_route53_record.this : k => v.fqdn },
    { for k, v in aws_route53_record.this_alias : k => v.fqdn }
  ) : null
}

output "precomputed" {
  value = {
    dnssec_kms_aws_kms_aliases = local.dnssec_should_create_kms_key ? module.dnssec_kms["0"].precomputed.aws_kms_aliases : null
  }
}
