# to be removed after 5+

moved {
  from = aws_kms_alias.this_dnssec["0"]
  to   = module.dnssec_kms["0"].aws_kms_alias.this["0"]
}

moved {
  from = aws_kms_key.this_dnssec["0"]
  to   = module.dnssec_kms["0"].aws_kms_key.this["0"]
}

# end to be removed after 5+
