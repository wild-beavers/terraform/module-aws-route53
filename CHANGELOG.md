## 4.0.0

- feat: (BREAKING) migrate the KMS key creation from local to the the wildbeavers kms module. Variables changes:
   - `var.current_region` => new
   - `var.current_account_id` => new
   - `var.testing_prefix` => new
   - `var.dnssec_kms_iam_policy_source_arns` => new
   - `var.dnssec_kms_iam_policy_entity_arns` => new
   - `var.dnssec_kms_iam_policy_sid_prefix` => new
   - `var.dnssec_kms_key` => new
   - `var.dnssec_kms_key_alias_name` => `var.dnssec_kms_key.alias`
   - `var.dnssec_kms_key_description` => `var.dnssec_kms_key.description`
   - `var.dnssec_kms_key_deletion_window_in_days` => `var.dnssec_kms_key.deletion_window_in_days`
   - `var.dnssec_kms_key_tags` => `var.dnssec_kms_key.tags`
   - `var.dnssec_external_key_enabled` => removed
- feat: (BREAKING) renames `var.dnssec_enable` => `var.dnssec_enabled`
- feat: (BREAKING) requires `aws.replica` provider
- feat: (BREAKING) renames `var.prefix` into `var.testing_prefix`
- feat: (BREAKING) changes outputs:
   - `dnssec_kms_key_id` => `dnssec_aws_kms_key.id`
   - `dnssec_kms_key_arn` => `dnssec_aws_kms_key.arn`
   - `dnssec_kms_key_alias_arn` => `dnssec_aws_kms_alias.arn`
   - `dnssec_ksk_ids[x]` => `aws_route53_key_signing_key[x].id`
   - `dnssec_ksk_records[x]` => `aws_route53_key_signing_key[x].dnskey_record`
   - `dnssec_ksk_ds_records[x]` => `aws_route53_key_signing_key[x].ds_record`
   - `dnssec_ksk_public_keys[x]` => `aws_route53_key_signing_key[x].public_key`
   - `dnssec_ksk_digest_values[x]` => `aws_route53_key_signing_key[x].digest_value`
   - `dnssec_ksk_digest_algorithm_types[x]` => `aws_route53_key_signing_key[x].digest_algorithm_type`
   - `dnssec_ksk_digest_algorithm_mnemonics[x]` => `aws_route53_key_signing_key[x].digest_algorithm_mnemonic`
   - `dnssec_ksk_signing_algorithm_type[x]` => `aws_route53_key_signing_key[x].signing_algorithm_type`
- feat: adds outputs `precomputed.dnssec_kms_aws_kms_aliases`
- fix: removes `Read permissions for everyone` statement for DNSSEC key, as it is useless and a security issue

## 3.1.1

- maintenance: migrate module to Terraform registry
- chore: bump pre-commit hooks

## 3.1.0

- feat: adds “var.dnssec_ksk_status”

## 3.0.2

- fix: idempotency issue with KMS associated to KSK. This now required a KMS key ID instead of KMS alias

## 3.0.1

- refactor: removes `lookup`s without alternative values
- test: changes deprecated `aws_subnet_ids` to `aws_subnets`

## 3.0.0

- maintenance: (BREAKING) bump the minimal Terraform version to 1.3+
- chore: bump pre-commit hooks
- test: remove Jenkinsfile

## 2.1.1

- maintenance: fix TFLINT issues
- chore: bump pre-commit hooks
- refactor: replace `count` by `for_each` when possible
- test: adds gitlab ci
- test: makes tests more robust

## 2.1.0

- feat: add `private_zone_domain_names` and `public_zone_domain_names` outputs
- chore: bump pre-commit hooks
- chore: replace deprecated `git` to `https` protocol for hooks URL

## 2.0.0

- refactor: wild-beaverize the module
- refactor: (BREAKING) modernize the module: heavily refactor the way zones are handled
- refactor: (BREAKING) modernize the module: heavily refactor the way records are handled
- refactor: (BREAKING) updates minimum requirements to terraform 1.0.5
- refactor: adds variables validations
- refactor: moves resolvers and forward rules to its own file
- refactor: changes forward rules and resolvers outputs to be null of non are created
- test: updates the examples in three: defaults, external and disable
- test: removes alternative examples: moved to default
- test: removes README in examples to ease the maintenance
- maintenance: updates pre-commit
- maintenance: updates .gitignore with latest practices

## 1.0.1

- Fix: pre-commit issues

## 1.0.0

- BREAKING : `rule_forward_resolver_target_ips` is now (object(list(string))) i.e `{"0" => ["127.0.0.1"]}`
- Convert from terraform 0.11 to 0.12
