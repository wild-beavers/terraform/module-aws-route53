#####
# General
#####

variable "tags" {
  description = "Tags to be shared among all resources of this module."
  type        = map(string)
  default     = {}
}

variable "vpc_id" {
  description = "ID of the VPC where to create resources for this module. Note that public hosted zones are not linked to any VPC, contrary to private zones."
  type        = string
  default     = null

  validation {
    condition     = var.vpc_id != null ? can(regex("^vpc-([a-z0-9]{8}|[a-z0-9]{17})$", var.vpc_id)) : true
    error_message = "The “var.vpc_id” does not match '^vpc-([a-z0-9]{8}|[a-z0-9]{17})$'."
  }
}

variable "current_region" {
  description = "The region where this module is run. If not provided, will be fetched dynamically. If given, this value is trusted and will not be checked against the real current region."
  default     = ""
  type        = string
  nullable    = false

  validation {
    condition     = can(regex("^([a-z]{2}-[a-z]{4,10}-[1-9]{1})?$", var.current_region))
    error_message = "“var.current_region” does not match '^([a-z]{2}-[a-z]{4,10}-[1-9]{1})?$'."
  }
}

variable "current_account_id" {
  description = "The account where this module is run. If not provided, will be fetched dynamically. If given, this value is trusted and will not be checked against the real current account ID."
  default     = ""
  type        = string
  nullable    = false

  validation {
    condition     = can(regex("^([0-9]{12})?$", var.current_account_id))
    error_message = "“var.current_account_id” does not match '^[0-9]{12}$'."
  }
}

variable "testing_prefix" {
  description = "Prefix to be used for all resources names. Specifically useful for tests. A 4-characters alphanumeric string. It must start by a lowercase letter."
  type        = string
  default     = ""
  nullable    = false

  validation {
    condition     = var.testing_prefix == "" || can(regex("^[a-z][a-zA-Z0-9]{3}$", var.testing_prefix))
    error_message = "“var.testing_prefix” does not match “^[a-z][a-zA-Z0-9]{3}$”."
  }
}

#####
# Hosted zone
#####

variable "public_zones" {
  type = list(object({
    logical_name   = string
    name           = string
    comment        = string
    prefix         = optional(string, "")
    delegation_set = optional(bool, false)
    dnssec         = optional(bool, true)
    tags           = optional(map(string))
  }))

  default = []

  description = <<-DOCUMENTATION
  List of route53 hosted public zones to create:
    * logical_name (required, string):         Name of the public zone (domain name). Careful, as names will be state file keys, they cannot be dynamic.
    * name (required, string):         Name of the public zone (domain name). Careful, as names will be state file keys, they cannot be dynamic.
    * comment (required, string):      Comment (description) of the public zone.
    * prefix (optional, string):       Prefix of the name. This value can by dynamic, contrary to the name.
    * delegation_set (optional, bool): Whether or not to create a delegation set with the public zone, to be able to reuse the NS servers. Defaults to `false`.
    * dnssec (optional, bool):         Whether or not to enable DNSSEC on this zone. Note this will only work for public zones. Makes sure `var.dnssec_enable` is enabled if you set this value to `true`. Defaults to `true`.
    * tags (optional, map(string)):    Specific tags for the zone to be merged with the default tags of the module `var.tags`.
DOCUMENTATION

  validation {
    condition = length(var.public_zones) == 0 || (
      !contains([for content in var.public_zones : (
        (length(content.logical_name) <= 1024 && length(content.logical_name) >= 1) &&
        (length(content.name) <= 1024 && length(content.name) >= 1)
      )], false)
    )
    error_message = "One or more “var.public_zones” are invalid. Check the requirements in the variables.tf file."
  }
}

variable "private_zones" {
  type = list(object({
    logical_name       = string
    name               = string
    comment            = string
    prefix             = optional(string, "")
    vpc_attachment_ids = optional(list(string), [])
    tags               = optional(map(string))
  }))

  default = []

  description = <<-DOCUMENTATION
  List of route53 hosted private zones to create. `name` as keys:
    * logical_name (required, string):             Logical name of the private zone. Careful, as logical names will be state file keys, they cannot be dynamic.
    * name (required, string):                     Name of the private zone (domain name).
    * comment (required, string):                  Comment (description) of the private zone.
    * tags (required, map(string)):                Specific tags for the zone to be merged with the default tags of the module `var.tags`.
    * prefix (optional, string):                   Prefix of the name.
    * vpc_attachment_ids (optional, list(string)): IDs of the VPC to be attached to the private hosted zone. This should not contain the `var.vpc_id` as it will be attached automatically.
DOCUMENTATION

  validation {
    condition = length(var.private_zones) == 0 || (
      !contains([for content in var.private_zones : (
        (length(content.logical_name) <= 1024 && length(content.logical_name) >= 1) &&
        (length(content.name) <= 1024 && length(content.name) >= 1)
      )], false)
    )
    error_message = "One or more “var.private_zones” are invalid. Check the requirements in the variables.tf file."
  }
}

variable "existing_zones" {
  type = list(object({
    logical_name       = string
    zone_id            = string
    vpc_attachment_ids = optional(list(string), [])
    dnssec             = optional(bool, false)
  }))

  default = []

  description = <<-DOCUMENTATION
  List of route53 hosted zones that already exists outside of this module:
    * logical_name (required, string):             Logical name of the private/public zone. Careful, as logical names will be state file keys, they cannot be dynamic.
    * zone_id (string):                            IDs of an existing hosted zone, public or private, to attach VPC and records.
    * vpc_attachment_ids (optional, list(string)): IDs of the VPC to be attached to the private hosted zones only. If this is specify with a public zone ID, it will throw an error.
    * dnssec (optional, bool):                     Whether or not to enable DNSSEC on this zone. Note this will only work for public zones. Makes sure `var.dnssec_enable` is enabled if you set this value to `true`. Defaults to `false`.
DOCUMENTATION

  validation {
    condition = length(var.existing_zones) == 0 || (
      !contains([for content in var.existing_zones : (
        (length(content.logical_name) <= 1024 && length(content.logical_name) >= 1) &&
        (can(regex("^Z([A-Z0-9]{20}|[A-Z0-9]{13})", content.zone_id)))
      )], false)
    )
    error_message = "One or more “var.existing_zones” are invalid. Check the requirements in the variables.tf file."
  }
}

#####
# Resolver endpoints inbound
#####

variable "resolver_tags" {
  description = "Tags specific to the resolvers to be created in the module. Will be merged with tags."
  type        = map(string)
  default     = {}
}

variable "resolver_inbound_count" {
  description = "How many INBOUND resolvers to be created in the module. This value cannot be computed automatically in Terraform 0.11."
  type        = number
  default     = 0
}

variable "resolver_inbound_names" {
  description = "Names of the INBOUND resolvers to be created in the module."
  type        = list(string)
  default     = []
}

variable "resolver_inbound_ip_addresses" {
  description = "Object of lists containing the IP addresses corresponding to the subnet IDs for the INBOUND resolvers to be created in the module. Look at examples for correct usage."
  type        = map(list(string))
  default     = {}
}

variable "resolver_inbound_subnet_ids" {
  description = "Object of lists containing the subnet IDs corresponding to the IP addresses for the INBOUND resolvers to be created in the module. Look at examples for correct usage."
  type        = map(list(string))
  default     = {}
}

variable "resolver_inbound_security_group_name" {
  description = "Name of the security groups shared for INBOUND resolvers."
  type        = string
  default     = "inbound-resolver"
}

variable "resolver_inbound_security_group_ingress_allowed_cidrs" {
  description = "CIDRs allowed to perform DNS request to the INBOUND resolvers, ingress rules."
  type        = list(string)
  default     = ["10.0.0.0/8"]
}

variable "resolver_inbound_security_group_egress_allowed_cidrs" {
  description = "CIDRs allowed to perform DNS request to the INBOUND resolvers, egress rules."
  type        = list(string)
  default     = ["10.0.0.0/8"]
}

#####
# Resolver endpoints outbound
#####

variable "resolver_outbound_count" {
  description = "How many OUTBOUND resolvers to be created in the module. This value cannot be computed automatically in Terraform 0.11."
  type        = number
  default     = 0
}

variable "resolver_outbound_names" {
  description = "Names of the OUTBOUND resolvers to be created in the module."
  type        = list(string)
  default     = []
}

variable "resolver_outbound_ip_addresses" {
  description = "Object of lists containing the IP addresses corresponding to the subnet IDs for the OUTBOUND resolvers to be created in the module. Look at examples for correct usage."
  type        = map(list(string))
  default     = {}
}

variable "resolver_outbound_subnet_ids" {
  description = "Object of lists containing the subnet IDs corresponding to the IP addresses for the OUTBOUND resolvers to be created in the module. Look at examples for correct usage."
  type        = map(list(string))
  default     = {}
}

variable "resolver_outbound_security_group_name" {
  description = "Name of the security groups shared for OUTBOUND resolvers."
  type        = string
  default     = "outbound-resolver"
}

variable "resolver_outbound_security_group_ingress_allowed_cidrs" {
  description = "CIDRs allowed to perform DNS request to the OUTBOUND resolvers, ingress rules."
  type        = list(string)
  default     = ["10.0.0.0/8"]
}

variable "resolver_outbound_security_group_egress_allowed_cidrs" {
  description = "CIDRs allowed to perform DNS request to the OUTBOUND resolvers, egress rules."
  type        = list(string)
  default     = ["10.0.0.0/8"]
}

#####
# Forward rules
#####

variable "rule_forward_count" {
  description = "How many resolvers forward rules to be created in the module. This value cannot be computed automatically in Terraform 0.11."
  type        = number
  default     = 0
}

variable "rule_forward_domain_names" {
  description = "Domain names of the resolvers forward rules to be created in the module. DNS queries for these domain names are forwarded to the IP addresses that are specified using target_ip."
  type        = list(string)
  default     = []
}

variable "rule_forward_names" {
  description = "Names of the resolvers forward rules to be created in the module. Friendly names that lets you easily find a rule in the Resolver dashboard in the Route 53 console."
  type        = list(string)
  default     = []
}

variable "rule_forward_resolver_endpoint_ids" {
  description = "IDs of the resolver endpoints to be used for the resolver forward rules. If not specify, the first OUBOUND resolver created by this module will be used for all the rules."
  type        = list(string)
  default     = []
}

variable "rule_forward_resolver_target_ips" {
  description = "Object of lists of objects containing target IPs for the resolver forward rules. IPs that you want resolvers to forward DNS queries to. Look at examples for correct usage."
  type        = map(list(string))
  default     = {}
}

variable "rule_forward_tags" {
  description = "Tags specific to the resolvers forward rules to be created in the module. Will be merged with tags."
  type        = map(string)
  default     = {}
}

variable "rule_forward_attachment_ids_count" {
  description = "How many var.rule_forward_attachment_ids. This value cannot be computed automatically in Terraform 0.11."
  type        = number
  default     = 0
}

variable "rule_forward_attachment_ids" {
  description = "IDs of the forward resolver rules that should be attached to the rule_forward_vpc_attachment_ids. If not specify, the forward rules created by this module will be used for all the attachments."
  type        = list(string)
  default     = []
}

variable "rule_forward_vpc_attachment_count" {
  description = "How many resolver forward rule attachments should be created in the module. This value cannot be computed automatically in Terraform 0.11."
  type        = number
  default     = 0
}

variable "rule_forward_vpc_attachment_ids" {
  description = "IDs of the VPC to be attached to the resolver forward rules of this module."
  type        = list(string)
  default     = []
}

#####
# Resource share
#####

variable "rule_forward_share_indexes" {
  description = "Indexes of the forward rules to be shared with other principals (rule_forward_share_principals). See examples for correct usage."
  type        = list(number)
  default     = []
}

variable "rule_forward_share_names" {
  description = "Names of the resource shares resolvers for forward rules to be created in the module."
  type        = list(string)
  default     = []
}

variable "rule_forward_share_tags" {
  description = "Tags specific to the resource shares for the forward rules to be created in the module. Will be merged with tags."
  type        = map(string)
  default     = {}
}

variable "rule_forward_share_principal_count" {
  description = "How many accounts must receive the resource shares for forward rules to be created in the module. This value cannot be computed automatically in Terraform 0.11."
  type        = number
  default     = 0
}

variable "rule_forward_share_principals" {
  description = "IDs of the accounts that must receive the resource shares for forward rules to be created in the module."
  type        = list(string)
  default     = []
}

#####
# Records
#####

variable "records" {
  type = list(object({
    zone_logical_name = string
    name              = string
    logical_name      = string
    type              = string
    ttl               = number
    records           = set(string)
  }))

  default = []

  description = <<-DOCUMENTATION
  List of records to create. Each record will be linked to the `zone_name` zone, looking up in the public zones first, then private zones.
    * zone_logical_name (required, string): Logical name of the zone where to create the record. Careful, this value cannot be dynamic, it's the `logical_name` chosen in `var.public_zones` or `var.private_zones`.
    * logical_name (required, string):      Logical name of the record. This value does not appear in the record itself. Careful, this value cannot be dynamic.
    * name (optional, string):              Record name (subdomain) to be added to the zone.
    * type (required, string):              The record type. Valid values are `A`, `AAAA`, `CAA`, `CNAME`, `DS`, `MX`, `NAPTR`, `NS`, `PTR`, `SOA`, `SPF`, `SRV` and `TXT`.
    * ttl (required, number):               The time-to-live of the record in seconds.
    * records (required, set(string)):      The record value. To specify a single record value longer than 255 characters such as a TXT record for DKIM, add `\"\"` inside the Terraform configuration string (e.g., \"first255characters\"\"morecharacters"`).
DOCUMENTATION

  validation {
    condition = length(var.records) == 0 || (
      !contains([for content in var.records : (
        (length(content.zone_logical_name) <= 1024 && length(content.zone_logical_name) >= 3) &&
        (length(content.logical_name) <= 1024 && length(content.logical_name) >= 1) &&
        (length(content.name) <= 1024 && length(content.name) >= 1) &&
        (contains(["A", "AAAA", "CAA", "CNAME", "DS", "MX", "NAPTR", "NS", "PTR", "SOA", "SPF", "SRV", "TXT"], content.type)) &&
        (content.ttl <= 2147483647 && content.ttl >= 0)
      )], false)
    )
    error_message = "One or more “var.records” are invalid. Check the requirements in the variables.tf file."
  }
}

variable "alias_records" {
  type = list(object({
    zone_logical_name            = string
    name                         = string
    logical_name                 = string
    type                         = string
    alias_name                   = string
    alias_zone_id                = string
    alias_evaluate_target_health = optional(bool, true)
  }))

  default = []

  description = <<-DOCUMENTATION
  List of records to create. Each record will be linked to the `zone_name` zone, looking up in the public zones first, then private zones.
    * zone_logical_name (required, string):          Logical name of the zone where to create the record. Careful, this value cannot be dynamic, it's the `logical_name` chosen in `var.public_zones` or `var.private_zones`.
    * logical_name (required, string):               Logical name of the record. This value does not appear in the record itself. Careful, this value cannot be dynamic.
    * name (required, string):                       Record name (subdomain) to be added to the zone.
    * type (required, string):                       The record type. Valid values are `A`, `AAAA`, `CAA`, `CNAME`, `DS`, `MX`, `NAPTR`, `NS`, `PTR`, `SOA`, `SPF`, `SRV` and `TXT`.
    * alias_name (required, string):                 DNS domain names for a CloudFront distribution, S3 bucket, ELB, or another resource record for the alias records to create.
    * alias_zone_id (required, string):              Zone ID of a CloudFront distribution, S3 bucket, ELB, or another resource for the alias records to create.
    * alias_evaluate_target_health (optional, bool): Set to true if you want Route 53 to determine whether to respond to DNS queries using this resource record set by checking the health of the resource record set. Default to `true`
DOCUMENTATION

  validation {
    condition = length(var.alias_records) == 0 || (
      !contains([for content in var.alias_records : (
        (length(content.zone_logical_name) <= 1024 && length(content.zone_logical_name) >= 3) &&
        (length(content.logical_name) <= 1024 && length(content.logical_name) >= 1) &&
        (length(content.name) <= 1024 && length(content.name) >= 1) &&
        (length(content.alias_name) <= 1024 && length(content.alias_name) >= 1) &&
        (can(regex("^Z([A-Z0-9]{20}|[A-Z0-9]{13})", content.alias_zone_id))) &&
        (contains(["A", "AAAA", "CAA", "CNAME", "DS", "MX", "NAPTR", "NS", "PTR", "SOA", "SPF", "SRV", "TXT"], content.type))
      )], false)
    )
    error_message = "One or more “var.alias_records” are invalid. Check the requirements in the variables.tf file."
  }
}
