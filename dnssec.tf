#####
# Variables
#####

variable "dnssec_enabled" {
  description = "Whether to enable DNSSEC. Will be applied to zones `var.public_zones` or `var.existing_zones` when they have the `dnssec` toggle to `true`."
  type        = bool
  default     = false
  nullable    = false

  validation {
    condition     = !var.dnssec_enabled || var.dnssec_kms_key != null || var.dnssec_external_kms_key_arn != ""
    error_message = "When “var.dnssec_enabled” is set, either “var.dnssec_kms_key” or “var.dnssec_external_kms_key_arn” is mandatory."
  }
  validation {
    condition     = !var.dnssec_enabled || !(var.dnssec_kms_key != null && var.dnssec_external_kms_key_arn != "")
    error_message = "“var.dnssec_kms_key” and “var.dnssec_external_kms_key_arn” are mutually exclusive."
  }
}

variable "dnssec_ksk_name" {
  description = "Name of the KSK keys to create for DNSSEC. One KSK per DNSSEC-enabled zone will be created. They can share the same name as they will share the same KMS key."
  type        = string
  default     = ""
  nullable    = false

  validation {
    condition     = var.dnssec_ksk_name != "" ? can(regex("^[a-zA-Z0-9_-]{3,128}$", var.dnssec_ksk_name)) : true
    error_message = "The “var.dnssec_ksk_name” does not match '^[a-zA-Z0-9_-]{3,128}$'."
  }
}

variable "dnssec_ksk_status" {
  description = "Status of the key-signing key (KSK). Valid values are “ACTIVE” or “INACTIVE”"
  default     = "ACTIVE"
  type        = string
  nullable    = false

  validation {
    condition     = contains(["ACTIVE", "INACTIVE"], var.dnssec_ksk_status)
    error_message = "“var.dnssec_ksk_status” must be one of “ACTIVE” or “INACTIVE”"
  }
}

variable "dnssec_external_kms_key_arn" {
  description = "ARN of an existing KMS key (ARN of the KMS key or alias) to be used for DNSSEC. The same key will be used for all the DNSSEC-enabled public zones."
  type        = string
  default     = ""
  nullable    = false

  validation {
    condition     = var.dnssec_external_kms_key_arn != "" ? can(regex("^arn:aws(-us-gov|-cn)?:kms:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:[0-9]{12}:(key/[a-z0-9-]{36}|alias/[a-zA-Z0-9:/_-]+)$", var.dnssec_external_kms_key_arn)) : true
    error_message = "The var.dnssec_external_kms_key_arn must match “^arn:aws(-us-gov|-cn)?:kms:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:[0-9]{12}:(key/[a-z0-9-]{36}|alias/[a-zA-Z0-9:/_-]+)$”."
  }
}

variable "dnssec_kms_key" {
  description = <<-DOCUMENTATION
Options of the DNSSEC KMS key to create.
If given, `var.dnssec_external_kms_key_arn` will be ignored.

  - alias                   (required, string):      KMS key alias; Display name of the KMS key. Omit the `alias/`.
  - policy_json             (optional, string):      Additional policy to attach to the KMS key, merged with a baseline internal policy. Make sure to provide "var.iam_admin_arns" to avoid getting blocked.
  - description             (optional, string, ""):  Description of the key.
  - rotation_enabled        (optional, bool, false): Whether to automatically rotate the KMS key linked to the DNSSEC key. By default, not available for signing key, this is expected to fail.
  - deletion_window_in_days (optional, number, 7):   The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between `7` and `30`, inclusive. If you do not specify a value, it defaults to `7`.
  - tags                    (optional, map(string)): Tags to be used by the KMS key of this module.
DOCUMENTATION
  type = object({
    alias                   = optional(string)
    policy_json             = optional(string, null)
    description             = optional(string, "")
    rotation_enabled        = optional(bool, false)
    deletion_window_in_days = optional(number, 7)
    tags                    = optional(map(string), {})
  })
  default = null
}

variable "dnssec_kms_iam_policy_sid_prefix" {
  description = "Use a prefix for all `Sid` of the DNSSEC KMS-related policies created by this module."
  type        = string
  default     = ""
  nullable    = false
}

variable "dnssec_kms_iam_policy_source_arns" {
  description = "Restrict access to the key to the given ARN sources. Wildcards are allowed. Allowed keys are `ro`, `rw`, `rwd` or `full`, each specifying the expected level of access for entities inside."
  type        = map(map(string))
  nullable    = false
  default     = {}
}

variable "dnssec_kms_iam_policy_entity_arns" {
  description = "Restrict access to the key to the given IAM entities (roles or users). Wildcards are allowed. Allowed keys are `ro`, `rw`, `rwd`, `audit` or `full`, each specifying the permission scope for entities inside them. If the same entity is inside multiple scope, the least privileged scope takes precedence over the most privileged."
  type        = map(map(string))
  nullable    = false
  default     = {}
}

variable "dnssec_kms_iam_policy_restrict_by_account_ids" {
  description = "Restrict resources created by this module by the given account IDs. Internal policies will whitelist given list. If `null`, no restriction will be applied. Especially useful when IAM entities contains accounts wildcards."
  type        = list(string)
  default     = []
  nullable    = false
}

variable "dnssec_kms_iam_policy_restrict_by_regions" {
  description = "Restrict resources created by this module by the given regions. Internal policies will whitelist given list. If `null`, no restriction will be applied. Especially useful when IAM entities contains region wildcards."
  type        = list(string)
  default     = []
  nullable    = false
}

#####
# Data
#####

data "aws_iam_policy_document" "this_dnssec" {
  for_each = local.dnssec_should_create_kms_key ? { 0 = 0 } : {}

  statement {
    sid    = "Allow Route 53 DNSSEC Service to CreateGrant"
    effect = "Allow"
    actions = [
      "kms:CreateGrant",
    ]
    resources = [
      "*"
    ]
    principals {
      type = "Service"
      identifiers = [
        "dnssec-route53.amazonaws.com"
      ]
    }
    condition {
      test     = "Bool"
      values   = ["true"]
      variable = "kms:GrantIsForAWSResource"
    }
  }
}

#####
# Resources
#####

locals {
  dnssec_should_create_kms_key = var.dnssec_enabled && var.dnssec_kms_key != null

  dnssec_zones_ids = concat(
    [for zone_logical_name, zone in local.public_zones :
      {
        zone_logical_name = zone.logical_name
        zone_id           = aws_route53_zone.public[zone.logical_name].id
      } if zone.dnssec
    ],
    [for zone_logical_name, zone in local.existing_zones :
      {
        zone_logical_name = zone.logical_name
        zone_id           = zone.zone_id
      } if zone.dnssec
    ],
  )
  dnssec_zones = { for zone in local.dnssec_zones_ids :
    zone.zone_logical_name => zone.zone_id
  }

  dnssec_kms_key_id = local.dnssec_should_create_kms_key ? module.dnssec_kms["0"].aws_kms_keys["0"].arn : var.dnssec_external_kms_key_arn
  dnssec_kms_iam_ro_actions = [
    "kms:DescribeKey",
    "kms:GetPublicKey",
  ]
  dnssec_kms_iam_rw_actions = concat(
    [
      "kms:Sign",
    ],
    local.dnssec_kms_iam_ro_actions
  )
  dnssec_kms_iam_audit_actions = [
    "kms:DescribeCustomKeyStores",
    "kms:DescribeKey",
    "kms:GetKeyPolicy",
    "kms:GetKeyRotationStatus",
    "kms:GetParametersForImport",
  ]
  dnssec_kms_iam_actions = {
    // https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/access-control-managing-permissions.html#KMS-key-policy-for-DNSSEC
    ro    = local.dnssec_kms_iam_ro_actions
    rw    = local.dnssec_kms_iam_rw_actions
    rwd   = local.dnssec_kms_iam_rw_actions
    audit = local.dnssec_kms_iam_audit_actions
    full  = ["kms:*"]
  }
}

module "dnssec_kms" {
  source  = "gitlab.com/wild-beavers/module-aws-kms/aws"
  version = "~> 2"

  for_each = local.dnssec_should_create_kms_key ? { 0 = var.dnssec_kms_key } : {}

  current_region     = var.current_region
  current_account_id = var.current_account_id
  prefix             = var.testing_prefix

  kms_keys = {
    0 = {
      alias             = each.value.alias
      principal_actions = local.dnssec_kms_iam_actions
      service_principal = "dnssec-route53"
      service_actions   = local.dnssec_kms_iam_actions["rw"]
      policy_json       = data.aws_iam_policy_document.this_dnssec["0"].json
      usage             = "SIGN_VERIFY"
      spec              = "ECC_NIST_P256"

      description             = each.value.description
      rotation_enabled        = each.value.rotation_enabled
      deletion_window_in_days = each.value.deletion_window_in_days
      tags                    = each.value.tags
    }
  }

  replica_enabled = false

  iam_policy_create                  = false
  iam_policy_export_json             = false
  iam_policy_export_actions          = false
  iam_policy_entity_arns             = var.dnssec_kms_iam_policy_entity_arns
  iam_policy_source_arns             = var.dnssec_kms_iam_policy_source_arns
  iam_policy_sid_prefix              = var.dnssec_kms_iam_policy_sid_prefix
  iam_policy_restrict_by_account_ids = var.dnssec_kms_iam_policy_restrict_by_account_ids
  iam_policy_restrict_by_regions     = var.dnssec_kms_iam_policy_restrict_by_regions

  tags = local.tags

  providers = {
    aws.replica = aws.replica
  }
}

resource "aws_route53_key_signing_key" "this" {
  for_each = var.dnssec_enabled ? local.dnssec_zones : {}

  hosted_zone_id             = each.value
  key_management_service_arn = local.dnssec_kms_key_id
  name                       = var.dnssec_ksk_name
  status                     = var.dnssec_ksk_status

  # This is to ensure the policy cannot be destroyed before the KMS policy
  depends_on = [module.dnssec_kms["0"].aws_kms_key_policy]
}

resource "aws_route53_hosted_zone_dnssec" "this" {
  for_each = var.dnssec_enabled ? local.dnssec_zones : {}

  hosted_zone_id = each.value

  depends_on = [
    aws_route53_key_signing_key.this
  ]
}

#####
# Outputs
#####

output "aws_route53_key_signing_key" {
  value = var.dnssec_enabled ? { for k, v in aws_route53_key_signing_key.this :
    k => v
  } : null
}

output "dnssec_aws_kms_key" {
  value = local.dnssec_should_create_kms_key ? module.dnssec_kms["0"].aws_kms_keys["0"] : null
}

output "dnssec_aws_kms_alias" {
  value = local.dnssec_should_create_kms_key ? module.dnssec_kms["0"].aws_kms_aliases["0"] : null
}
