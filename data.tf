data "aws_subnet" "this_inbound" {
  count = var.resolver_inbound_count

  id = element(var.resolver_inbound_subnet_ids[count.index], 0)
}

data "aws_subnet" "this_outbound" {
  count = var.resolver_outbound_count

  id = element(var.resolver_outbound_subnet_ids[count.index], 0)
}
