# Terraform Route53 module

This module is not to be deployed directly.

## Limitations

- Records cannot be added in a private and public zone that have the exact same name.
  If it is the case, the record will be added to the first matching zone ID found in this list: 1. existing zone 2. public zone created by the module 3. private zone created by the module.
  To work around this issue, you can call the module multiple times.
- All resolvers of the same type shares the same security group rules.
- All resolvers must have an exact number of 2 IPs. This cannot be worked around in Terraform 0.11.
- There is no way to auto accept resource shares with the aws provider 2.18.
  That’s why the resource shares created with this module must be accepted manually on receiving accounts.
- AWS does not offer a way to auto-accept zones associations between account.
  In the case zones must be shared between VPCs of different account, this should be done manually until this is solved: <https://github.com/terraform-providers/terraform-provider-aws/issues/617>.
  See this <https://docs.aws.amazon.com/cli/latest/reference/route53/create-vpc-association-authorization.html> for manual handling.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.9 |
| aws | >= 3.15 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 3.15 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| dnssec\_kms | gitlab.com/wild-beavers/module-aws-kms/aws | ~> 2 |

## Resources

| Name | Type |
|------|------|
| [aws_ram_principal_association.this_forward](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ram_principal_association) | resource |
| [aws_ram_resource_association.this_forward](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ram_resource_association) | resource |
| [aws_ram_resource_share.this_forward](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ram_resource_share) | resource |
| [aws_route53_delegation_set.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_delegation_set) | resource |
| [aws_route53_hosted_zone_dnssec.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_hosted_zone_dnssec) | resource |
| [aws_route53_key_signing_key.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_key_signing_key) | resource |
| [aws_route53_record.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_route53_record.this_alias](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_route53_resolver_endpoint.this_inbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_resolver_endpoint) | resource |
| [aws_route53_resolver_endpoint.this_outbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_resolver_endpoint) | resource |
| [aws_route53_resolver_rule.this_forward](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_resolver_rule) | resource |
| [aws_route53_resolver_rule_association.this_forward](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_resolver_rule_association) | resource |
| [aws_route53_zone.private](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone) | resource |
| [aws_route53_zone.public](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone) | resource |
| [aws_route53_zone_association.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone_association) | resource |
| [aws_security_group.this_inbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group.this_outbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.this_inbound_53_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_inbound_53_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_inbound_853_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_inbound_853_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_inbound_out_53_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_inbound_out_53_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_inbound_out_853_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_inbound_out_853_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_outbound_53_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_outbound_53_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_outbound_853_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_outbound_853_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_outbound_out_53_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_outbound_out_53_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_outbound_out_853_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_outbound_out_853_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_iam_policy_document.this_dnssec](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_subnet.this_inbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet) | data source |
| [aws_subnet.this_outbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| alias\_records | List of records to create. Each record will be linked to the `zone_name` zone, looking up in the public zones first, then private zones.<br/>  * zone\_logical\_name (required, string):          Logical name of the zone where to create the record. Careful, this value cannot be dynamic, it's the `logical_name` chosen in `var.public_zones` or `var.private_zones`.<br/>  * logical\_name (required, string):               Logical name of the record. This value does not appear in the record itself. Careful, this value cannot be dynamic.<br/>  * name (required, string):                       Record name (subdomain) to be added to the zone.<br/>  * type (required, string):                       The record type. Valid values are `A`, `AAAA`, `CAA`, `CNAME`, `DS`, `MX`, `NAPTR`, `NS`, `PTR`, `SOA`, `SPF`, `SRV` and `TXT`.<br/>  * alias\_name (required, string):                 DNS domain names for a CloudFront distribution, S3 bucket, ELB, or another resource record for the alias records to create.<br/>  * alias\_zone\_id (required, string):              Zone ID of a CloudFront distribution, S3 bucket, ELB, or another resource for the alias records to create.<br/>  * alias\_evaluate\_target\_health (optional, bool): Set to true if you want Route 53 to determine whether to respond to DNS queries using this resource record set by checking the health of the resource record set. Default to `true` | <pre>list(object({<br/>    zone_logical_name            = string<br/>    name                         = string<br/>    logical_name                 = string<br/>    type                         = string<br/>    alias_name                   = string<br/>    alias_zone_id                = string<br/>    alias_evaluate_target_health = optional(bool, true)<br/>  }))</pre> | `[]` | no |
| current\_account\_id | The account where this module is run. If not provided, will be fetched dynamically. If given, this value is trusted and will not be checked against the real current account ID. | `string` | `""` | no |
| current\_region | The region where this module is run. If not provided, will be fetched dynamically. If given, this value is trusted and will not be checked against the real current region. | `string` | `""` | no |
| dnssec\_enabled | Whether to enable DNSSEC. Will be applied to zones `var.public_zones` or `var.existing_zones` when they have the `dnssec` toggle to `true`. | `bool` | `false` | no |
| dnssec\_external\_kms\_key\_arn | ARN of an existing KMS key (ARN of the KMS key or alias) to be used for DNSSEC. The same key will be used for all the DNSSEC-enabled public zones. | `string` | `""` | no |
| dnssec\_kms\_iam\_policy\_entity\_arns | Restrict access to the key to the given IAM entities (roles or users). Wildcards are allowed. Allowed keys are `ro`, `rw`, `rwd`, `audit` or `full`, each specifying the permission scope for entities inside them. If the same entity is inside multiple scope, the least privileged scope takes precedence over the most privileged. | `map(map(string))` | `{}` | no |
| dnssec\_kms\_iam\_policy\_restrict\_by\_account\_ids | Restrict resources created by this module by the given account IDs. Internal policies will whitelist given list. If `null`, no restriction will be applied. Especially useful when IAM entities contains accounts wildcards. | `list(string)` | `[]` | no |
| dnssec\_kms\_iam\_policy\_restrict\_by\_regions | Restrict resources created by this module by the given regions. Internal policies will whitelist given list. If `null`, no restriction will be applied. Especially useful when IAM entities contains region wildcards. | `list(string)` | `[]` | no |
| dnssec\_kms\_iam\_policy\_sid\_prefix | Use a prefix for all `Sid` of the DNSSEC KMS-related policies created by this module. | `string` | `""` | no |
| dnssec\_kms\_iam\_policy\_source\_arns | Restrict access to the key to the given ARN sources. Wildcards are allowed. Allowed keys are `ro`, `rw`, `rwd` or `full`, each specifying the expected level of access for entities inside. | `map(map(string))` | `{}` | no |
| dnssec\_kms\_key | Options of the DNSSEC KMS key to create.<br/>If given, `var.dnssec_external_kms_key_arn` will be ignored.<br/><br/>  - alias                   (required, string):      KMS key alias; Display name of the KMS key. Omit the `alias/`.<br/>  - policy\_json             (optional, string):      Additional policy to attach to the KMS key, merged with a baseline internal policy. Make sure to provide "var.iam\_admin\_arns" to avoid getting blocked.<br/>  - description             (optional, string, ""):  Description of the key.<br/>  - rotation\_enabled        (optional, bool, false): Whether to automatically rotate the KMS key linked to the DNSSEC key. By default, not available for signing key, this is expected to fail.<br/>  - deletion\_window\_in\_days (optional, number, 7):   The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between `7` and `30`, inclusive. If you do not specify a value, it defaults to `7`.<br/>  - tags                    (optional, map(string)): Tags to be used by the KMS key of this module. | <pre>object({<br/>    alias                   = optional(string)<br/>    policy_json             = optional(string, null)<br/>    description             = optional(string, "")<br/>    rotation_enabled        = optional(bool, false)<br/>    deletion_window_in_days = optional(number, 7)<br/>    tags                    = optional(map(string), {})<br/>  })</pre> | `null` | no |
| dnssec\_ksk\_name | Name of the KSK keys to create for DNSSEC. One KSK per DNSSEC-enabled zone will be created. They can share the same name as they will share the same KMS key. | `string` | `""` | no |
| dnssec\_ksk\_status | Status of the key-signing key (KSK). Valid values are “ACTIVE” or “INACTIVE” | `string` | `"ACTIVE"` | no |
| existing\_zones | List of route53 hosted zones that already exists outside of this module:<br/>  * logical\_name (required, string):             Logical name of the private/public zone. Careful, as logical names will be state file keys, they cannot be dynamic.<br/>  * zone\_id (string):                            IDs of an existing hosted zone, public or private, to attach VPC and records.<br/>  * vpc\_attachment\_ids (optional, list(string)): IDs of the VPC to be attached to the private hosted zones only. If this is specify with a public zone ID, it will throw an error.<br/>  * dnssec (optional, bool):                     Whether or not to enable DNSSEC on this zone. Note this will only work for public zones. Makes sure `var.dnssec_enable` is enabled if you set this value to `true`. Defaults to `false`. | <pre>list(object({<br/>    logical_name       = string<br/>    zone_id            = string<br/>    vpc_attachment_ids = optional(list(string), [])<br/>    dnssec             = optional(bool, false)<br/>  }))</pre> | `[]` | no |
| private\_zones | List of route53 hosted private zones to create. `name` as keys:<br/>  * logical\_name (required, string):             Logical name of the private zone. Careful, as logical names will be state file keys, they cannot be dynamic.<br/>  * name (required, string):                     Name of the private zone (domain name).<br/>  * comment (required, string):                  Comment (description) of the private zone.<br/>  * tags (required, map(string)):                Specific tags for the zone to be merged with the default tags of the module `var.tags`.<br/>  * prefix (optional, string):                   Prefix of the name.<br/>  * vpc\_attachment\_ids (optional, list(string)): IDs of the VPC to be attached to the private hosted zone. This should not contain the `var.vpc_id` as it will be attached automatically. | <pre>list(object({<br/>    logical_name       = string<br/>    name               = string<br/>    comment            = string<br/>    prefix             = optional(string, "")<br/>    vpc_attachment_ids = optional(list(string), [])<br/>    tags               = optional(map(string))<br/>  }))</pre> | `[]` | no |
| public\_zones | List of route53 hosted public zones to create:<br/>  * logical\_name (required, string):         Name of the public zone (domain name). Careful, as names will be state file keys, they cannot be dynamic.<br/>  * name (required, string):         Name of the public zone (domain name). Careful, as names will be state file keys, they cannot be dynamic.<br/>  * comment (required, string):      Comment (description) of the public zone.<br/>  * prefix (optional, string):       Prefix of the name. This value can by dynamic, contrary to the name.<br/>  * delegation\_set (optional, bool): Whether or not to create a delegation set with the public zone, to be able to reuse the NS servers. Defaults to `false`.<br/>  * dnssec (optional, bool):         Whether or not to enable DNSSEC on this zone. Note this will only work for public zones. Makes sure `var.dnssec_enable` is enabled if you set this value to `true`. Defaults to `true`.<br/>  * tags (optional, map(string)):    Specific tags for the zone to be merged with the default tags of the module `var.tags`. | <pre>list(object({<br/>    logical_name   = string<br/>    name           = string<br/>    comment        = string<br/>    prefix         = optional(string, "")<br/>    delegation_set = optional(bool, false)<br/>    dnssec         = optional(bool, true)<br/>    tags           = optional(map(string))<br/>  }))</pre> | `[]` | no |
| records | List of records to create. Each record will be linked to the `zone_name` zone, looking up in the public zones first, then private zones.<br/>  * zone\_logical\_name (required, string): Logical name of the zone where to create the record. Careful, this value cannot be dynamic, it's the `logical_name` chosen in `var.public_zones` or `var.private_zones`.<br/>  * logical\_name (required, string):      Logical name of the record. This value does not appear in the record itself. Careful, this value cannot be dynamic.<br/>  * name (optional, string):              Record name (subdomain) to be added to the zone.<br/>  * type (required, string):              The record type. Valid values are `A`, `AAAA`, `CAA`, `CNAME`, `DS`, `MX`, `NAPTR`, `NS`, `PTR`, `SOA`, `SPF`, `SRV` and `TXT`.<br/>  * ttl (required, number):               The time-to-live of the record in seconds.<br/>  * records (required, set(string)):      The record value. To specify a single record value longer than 255 characters such as a TXT record for DKIM, add `\"\"` inside the Terraform configuration string (e.g., \"first255characters\"\"morecharacters"`).<br/>` | <pre>list(object({<br/>    zone_logical_name = string<br/>    name              = string<br/>    logical_name      = string<br/>    type              = string<br/>    ttl               = number<br/>    records           = set(string)<br/>  }))</pre> | `[]` | no |
| resolver\_inbound\_count | How many INBOUND resolvers to be created in the module. This value cannot be computed automatically in Terraform 0.11. | `number` | `0` | no |
| resolver\_inbound\_ip\_addresses | Object of lists containing the IP addresses corresponding to the subnet IDs for the INBOUND resolvers to be created in the module. Look at examples for correct usage. | `map(list(string))` | `{}` | no |
| resolver\_inbound\_names | Names of the INBOUND resolvers to be created in the module. | `list(string)` | `[]` | no |
| resolver\_inbound\_security\_group\_egress\_allowed\_cidrs | CIDRs allowed to perform DNS request to the INBOUND resolvers, egress rules. | `list(string)` | <pre>[<br/>  "10.0.0.0/8"<br/>]</pre> | no |
| resolver\_inbound\_security\_group\_ingress\_allowed\_cidrs | CIDRs allowed to perform DNS request to the INBOUND resolvers, ingress rules. | `list(string)` | <pre>[<br/>  "10.0.0.0/8"<br/>]</pre> | no |
| resolver\_inbound\_security\_group\_name | Name of the security groups shared for INBOUND resolvers. | `string` | `"inbound-resolver"` | no |
| resolver\_inbound\_subnet\_ids | Object of lists containing the subnet IDs corresponding to the IP addresses for the INBOUND resolvers to be created in the module. Look at examples for correct usage. | `map(list(string))` | `{}` | no |
| resolver\_outbound\_count | How many OUTBOUND resolvers to be created in the module. This value cannot be computed automatically in Terraform 0.11. | `number` | `0` | no |
| resolver\_outbound\_ip\_addresses | Object of lists containing the IP addresses corresponding to the subnet IDs for the OUTBOUND resolvers to be created in the module. Look at examples for correct usage. | `map(list(string))` | `{}` | no |
| resolver\_outbound\_names | Names of the OUTBOUND resolvers to be created in the module. | `list(string)` | `[]` | no |
| resolver\_outbound\_security\_group\_egress\_allowed\_cidrs | CIDRs allowed to perform DNS request to the OUTBOUND resolvers, egress rules. | `list(string)` | <pre>[<br/>  "10.0.0.0/8"<br/>]</pre> | no |
| resolver\_outbound\_security\_group\_ingress\_allowed\_cidrs | CIDRs allowed to perform DNS request to the OUTBOUND resolvers, ingress rules. | `list(string)` | <pre>[<br/>  "10.0.0.0/8"<br/>]</pre> | no |
| resolver\_outbound\_security\_group\_name | Name of the security groups shared for OUTBOUND resolvers. | `string` | `"outbound-resolver"` | no |
| resolver\_outbound\_subnet\_ids | Object of lists containing the subnet IDs corresponding to the IP addresses for the OUTBOUND resolvers to be created in the module. Look at examples for correct usage. | `map(list(string))` | `{}` | no |
| resolver\_tags | Tags specific to the resolvers to be created in the module. Will be merged with tags. | `map(string)` | `{}` | no |
| rule\_forward\_attachment\_ids | IDs of the forward resolver rules that should be attached to the rule\_forward\_vpc\_attachment\_ids. If not specify, the forward rules created by this module will be used for all the attachments. | `list(string)` | `[]` | no |
| rule\_forward\_attachment\_ids\_count | How many var.rule\_forward\_attachment\_ids. This value cannot be computed automatically in Terraform 0.11. | `number` | `0` | no |
| rule\_forward\_count | How many resolvers forward rules to be created in the module. This value cannot be computed automatically in Terraform 0.11. | `number` | `0` | no |
| rule\_forward\_domain\_names | Domain names of the resolvers forward rules to be created in the module. DNS queries for these domain names are forwarded to the IP addresses that are specified using target\_ip. | `list(string)` | `[]` | no |
| rule\_forward\_names | Names of the resolvers forward rules to be created in the module. Friendly names that lets you easily find a rule in the Resolver dashboard in the Route 53 console. | `list(string)` | `[]` | no |
| rule\_forward\_resolver\_endpoint\_ids | IDs of the resolver endpoints to be used for the resolver forward rules. If not specify, the first OUBOUND resolver created by this module will be used for all the rules. | `list(string)` | `[]` | no |
| rule\_forward\_resolver\_target\_ips | Object of lists of objects containing target IPs for the resolver forward rules. IPs that you want resolvers to forward DNS queries to. Look at examples for correct usage. | `map(list(string))` | `{}` | no |
| rule\_forward\_share\_indexes | Indexes of the forward rules to be shared with other principals (rule\_forward\_share\_principals). See examples for correct usage. | `list(number)` | `[]` | no |
| rule\_forward\_share\_names | Names of the resource shares resolvers for forward rules to be created in the module. | `list(string)` | `[]` | no |
| rule\_forward\_share\_principal\_count | How many accounts must receive the resource shares for forward rules to be created in the module. This value cannot be computed automatically in Terraform 0.11. | `number` | `0` | no |
| rule\_forward\_share\_principals | IDs of the accounts that must receive the resource shares for forward rules to be created in the module. | `list(string)` | `[]` | no |
| rule\_forward\_share\_tags | Tags specific to the resource shares for the forward rules to be created in the module. Will be merged with tags. | `map(string)` | `{}` | no |
| rule\_forward\_tags | Tags specific to the resolvers forward rules to be created in the module. Will be merged with tags. | `map(string)` | `{}` | no |
| rule\_forward\_vpc\_attachment\_count | How many resolver forward rule attachments should be created in the module. This value cannot be computed automatically in Terraform 0.11. | `number` | `0` | no |
| rule\_forward\_vpc\_attachment\_ids | IDs of the VPC to be attached to the resolver forward rules of this module. | `list(string)` | `[]` | no |
| tags | Tags to be shared among all resources of this module. | `map(string)` | `{}` | no |
| testing\_prefix | Prefix to be used for all resources names. Specifically useful for tests. A 4-characters alphanumeric string. It must start by a lowercase letter. | `string` | `""` | no |
| vpc\_id | ID of the VPC where to create resources for this module. Note that public hosted zones are not linked to any VPC, contrary to private zones. | `string` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| aws\_route53\_key\_signing\_key | n/a |
| delegation\_set\_arns | n/a |
| delegation\_set\_ids | n/a |
| delegation\_set\_name\_servers | n/a |
| dnssec\_aws\_kms\_alias | n/a |
| dnssec\_aws\_kms\_key | n/a |
| precomputed | n/a |
| principal\_association\_forward\_id | n/a |
| private\_zone\_arns | n/a |
| private\_zone\_domain\_names | n/a |
| private\_zone\_ids | n/a |
| private\_zone\_name\_servers | n/a |
| public\_zone\_arns | n/a |
| public\_zone\_domain\_names | n/a |
| public\_zone\_ids | n/a |
| public\_zone\_name\_servers | n/a |
| record\_fqdns | n/a |
| record\_names | n/a |
| resolver\_inbound\_arns | n/a |
| resolver\_inbound\_host\_vpc\_ids | n/a |
| resolver\_inbound\_ids | n/a |
| resolver\_inbound\_security\_group\_ids | n/a |
| resolver\_outbound\_arns | n/a |
| resolver\_outbound\_host\_vpc\_ids | n/a |
| resolver\_outbound\_ids | n/a |
| resolver\_outbound\_security\_group\_id | n/a |
| resource\_association\_forward\_id | n/a |
| rule\_association\_forward\_id | n/a |
| rule\_forward\_arns | n/a |
| rule\_forward\_ids | n/a |
| rule\_forward\_owner\_ids | n/a |
| rule\_forward\_share\_arns | n/a |
| rule\_forward\_share\_ids | n/a |
| rule\_forward\_share\_statuses | n/a |
<!-- END_TF_DOCS -->
