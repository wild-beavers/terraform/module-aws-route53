output "default_private" {
  value = module.default_private
}

output "default_public" {
  value = module.default_public
}

output "complete" {
  value = module.complete
}
