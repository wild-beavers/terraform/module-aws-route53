#####
# Randoms
#####

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

locals {
  prefix = "${random_string.start_letter.result}${random_string.this.result}"
}

#####
# Baseline
#####

data "aws_vpc" "default" {
  default = true
}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_subnets" "all" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }

  filter {
    name   = "availability-zone"
    values = data.aws_availability_zones.available.names
  }

  filter {
    name   = "default-for-az"
    values = ["true"]
  }
}

data "aws_subnet" "default" {
  for_each = toset(data.aws_subnets.all.ids)

  id = each.key
}

locals {
  subnet_ids        = [for k, v in data.aws_subnet.default : k]
  subnet_cidrs      = [for k, v in data.aws_subnet.default : v.cidr_block]
  caller_origin_arn = replace(replace(join("/", chunklist(split("/", data.aws_caller_identity.current.arn), 2)[0]), "sts", "iam"), "assumed-role", "role")
}

resource "aws_vpc" "main" {
  cidr_block = "10.40.20.0/24"
}

resource "aws_vpc" "second" {
  cidr_block       = "10.40.30.0/24"
  instance_tenancy = "dedicated"
}

resource "aws_route53_zone" "private" {
  name = "${local.prefix}.private.tftest.com"

  vpc {
    vpc_id = data.aws_vpc.default.id
  }

  lifecycle {
    ignore_changes = [vpc]
  }
}

resource "aws_route53_zone" "public" {
  name = "${local.prefix}.public.tftest.com"
}

resource "aws_lb" "test" {
  name               = "${local.prefix}tftest"
  internal           = true
  load_balancer_type = "network"
  subnets            = [for subnet in data.aws_subnet.default : subnet.id]
}

#####
# Default simple example
# Shows how to:
# - create diverse private zones
# - attach VPCs private zones
#####

module "default_private" {
  source = "../../"

  current_account_id = data.aws_caller_identity.current.account_id
  current_region     = data.aws_region.current.name
  testing_prefix     = local.prefix
  vpc_id             = data.aws_vpc.default.id

  private_zones = [
    {
      logical_name       = "vpc-attachments.private.tftest.com"
      name               = "vpc-attachments.private.tftest.com"
      comment            = ".vpc-attachments.private.tftest.com zone for terraform test"
      vpc_attachment_ids = [aws_vpc.main.id, aws_vpc.second.id]
      tags = {
        test = "with multiple VPC attachment"
      }
    },
    {
      logical_name = "simple.private.tftest.com"
      name         = "simple.private.tftest.com"
      comment      = ".simple.private.tftest.com zone for terraform test"
      tags = {
        test = "simple"
      }
    },
    {
      logical_name = "prefix.private.tftest.com"
      name         = ".prefix.private.tftest.com"
      comment      = ".prefix.private.tftest.com zone for terraform test"
      prefix       = local.prefix
      tags = {
        test = "prefix"
      }
    },
  ]

  tags = {
    why = "for terraform test"
  }

  providers = {
    aws.replica = aws.replica
  }
}

#####
# Default simple example
# Shows how to:
# - create diverse public zones
# - enable DNSSEC
# - Create DNS key in the module
# - Create a delegation set for a public zone
#####

module "default_public" {
  source = "../../"

  current_account_id = data.aws_caller_identity.current.account_id
  current_region     = data.aws_region.current.name
  testing_prefix     = local.prefix
  vpc_id             = data.aws_vpc.default.id
  tags = {
    why = "for terraform test"
  }

  public_zones = [
    {
      logical_name = "prefix.public.tftest.com"
      name         = ".prefix.public.tftest.com"
      prefix       = data.aws_region.current.name
      comment      = "${data.aws_region.current.name}.prefix.public.tftest.com zone for terraform test"
      tags = {
        test = "dynamic prefix"
      }
    },
    {
      logical_name   = "delegation-set1.public.tftest.com"
      name           = "delegation-set1.public.tftest.com"
      comment        = ".delegation-set1.public.tftest.com zone for terraform test"
      delegation_set = true
      tags = {
        number = "delegated 1"
      }
    },
    {
      logical_name   = "delegation-set2.public.tftest.com"
      name           = "delegation-set2.public.tftest.com"
      comment        = ".delegation-set2.public.tftest.com zone for terraform test"
      delegation_set = true
      tags = {
        number = "delegated 2"
      }
    },
  ]

  dnssec_enabled  = true
  dnssec_ksk_name = "${local.prefix}ksktftest"
  dnssec_kms_iam_policy_entity_arns = {
    full = {
      caller = local.caller_origin_arn
    }
  }
  dnssec_kms_key = {
    alias                   = "${local.prefix}aliastftest"
    description             = "A DNSSEC key"
    deletion_window_in_days = 8
    tags = {
      test = "create DNSSEC key for public hosted zones"
    }
  }

  providers = {
    aws.replica = aws.replica
  }
}

#####
# Complete example
# Shows how to:
# - existing public/private zones
# - use resolvers
# - use forward rules
# - adds records and aliases to the hosted zones
#####

module "complete" {
  source = "../../"

  current_account_id = data.aws_caller_identity.current.account_id
  current_region     = data.aws_region.current.name
  testing_prefix     = local.prefix
  vpc_id             = data.aws_vpc.default.id

  existing_zones = [
    {
      logical_name = "private.tftest.com"
      zone_id      = aws_route53_zone.private.zone_id
    },
    {
      logical_name = "public.tftest.com"
      zone_id      = aws_route53_zone.public.zone_id
    },
  ]

  private_zones = [
    {
      logical_name = "resolver.private.tftest.com"
      name         = ".resolver.private.tftest.com"
      comment      = "resolver.private.tftest.com zone for terraform test"
      tags = {
        test = "resolver and records"
      }
    },
  ]

  public_zones = [
    {
      logical_name = "dnssec-disabled.public.tftest.com"
      name         = ".dnssec-disabled.public.tftest.com"
      comment      = ".dnssec-disabled.public.tftest.com zone for terraform test"
      dnssec       = false
      tags = {
        test = "DNSSEC disabled and records"
      }
    },
    {
      logical_name = "dnssec-enabled.public.tftest.com"
      name         = ".dnssec-enabled.public.tftest.com"
      comment      = ".dnssec-enabled.public.tftest.com zone for terraform test"
      dnssec       = true
      tags = {
        test = "DNSSEC enabled and records"
      }
    },
  ]

  dnssec_enabled  = true
  dnssec_ksk_name = "${local.prefix}kskcompletetftest"
  dnssec_kms_iam_policy_entity_arns = {
    full = {
      caller = local.caller_origin_arn
    }
  }
  dnssec_kms_key = {
    alias                   = "${local.prefix}aliascompletetftest"
    description             = "A DNSSEC key for complete test"
    deletion_window_in_days = 8
    tags = {
      test = "create DNSSEC key for public hosted zones"
    }
  }

  resolver_tags = {
    Name = "${local.prefix}tftest"
  }
  resolver_inbound_count = 1
  resolver_inbound_names = ["${local.prefix}inResolver"]
  resolver_inbound_ip_addresses = {
    0 = [
      cidrhost(local.subnet_cidrs[0], 15),
      cidrhost(local.subnet_cidrs[1], 16),
    ]
  }
  resolver_inbound_subnet_ids = {
    0 = local.subnet_ids
  }
  resolver_inbound_security_group_name                  = "${local.prefix}inResolver"
  resolver_inbound_security_group_ingress_allowed_cidrs = ["192.168.0.0/16", "10.0.0.0/8"]
  resolver_inbound_security_group_egress_allowed_cidrs  = ["10.0.0.0/8"]

  resolver_outbound_count = 1
  resolver_outbound_names = ["${local.prefix}outResolver"]
  resolver_outbound_ip_addresses = {
    0 = [
      cidrhost(local.subnet_cidrs[0], 10),
      cidrhost(local.subnet_cidrs[1], 11),
    ]
  }
  resolver_outbound_subnet_ids = {
    0 = local.subnet_ids
  }
  resolver_outbound_security_group_name                  = "${local.prefix}outResolver"
  resolver_outbound_security_group_ingress_allowed_cidrs = ["192.168.0.0/16", "10.0.0.0/8"]
  resolver_outbound_security_group_egress_allowed_cidrs  = ["192.168.0.0/16", "10.0.0.0/8"]

  rule_forward_share_indexes = []
  rule_forward_count         = 1
  rule_forward_domain_names  = ["${local.prefix}.tftest-rule-example.com"]
  rule_forward_names         = ["${local.prefix}ruleForward"]
  rule_forward_resolver_target_ips = {
    0 = [
      "123.45.67.5",
      "123.45.68.5"
    ]
  }
  rule_forward_tags = {
    Name = "${local.prefix}tftest"
  }
  rule_forward_vpc_attachment_count = 3
  rule_forward_vpc_attachment_ids   = [data.aws_vpc.default.id, aws_vpc.main.id, aws_vpc.second.id]

  records = [
    {
      zone_logical_name = "resolver.private.tftest.com"
      name              = "record1"
      logical_name      = "record1"
      type              = "A"
      ttl               = 10
      records           = ["172.31.2.10"]
    },
    {
      zone_logical_name = "resolver.private.tftest.com"
      name              = "r1.prefix"
      logical_name      = "prefix"
      type              = "CNAME"
      ttl               = 30
      records           = ["${local.prefix}record3.${local.prefix}.private.tftest.com"]
    },
    {
      zone_logical_name = "private.tftest.com"
      logical_name      = "record3"
      name              = "record3"
      type              = "A"
      ttl               = 100
      records           = ["1.2.3.4"]
    },
    {
      zone_logical_name = "public.tftest.com"
      logical_name      = "record4"
      name              = "record4"
      type              = "A"
      ttl               = 3600
      records           = ["1.2.3.4"]
    },
  ]

  alias_records = [
    {
      zone_logical_name = "public.tftest.com"
      logical_name      = "alias"
      name              = "alias"
      type              = "A"
      alias_name        = aws_lb.test.dns_name
      alias_zone_id     = aws_lb.test.zone_id
    }
  ]

  providers = {
    aws.replica = aws.replica
  }
}
