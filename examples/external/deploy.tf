#####
# Randoms
#####

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

locals {
  prefix = "${random_string.start_letter.result}${random_string.this.result}"
}

#####
# Baseline
#####

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

data "aws_vpc" "default" {
  default = true
}

data "aws_iam_policy_document" "dnssec" {
  statement {
    sid    = "Read permissions for everyone dont do that in prod"
    effect = "Allow"
    actions = [
      "kms:*",
    ]
    resources = [
      "*"
    ]
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
  }
}

resource "aws_kms_key" "example" {
  customer_master_key_spec = "ECC_NIST_P256"
  deletion_window_in_days  = 7
  key_usage                = "SIGN_VERIFY"

  policy = data.aws_iam_policy_document.dnssec.json
}

resource "aws_vpc" "main" {
  cidr_block = "10.40.20.0/24"
}

resource "aws_route53_zone" "private" {
  name = "${local.prefix}.private.tftest.com"

  vpc {
    vpc_id = data.aws_vpc.default.id
  }

  lifecycle {
    ignore_changes = [vpc]
  }
}

resource "aws_route53_zone" "public" {
  name = "${local.prefix}.public.tftest.com"
}

#####
# External example
# Shows how to:
# - add records to existing public/private zones
# - link VPC to existing private zone
# - enable DNSSEC on existing public zone
# - use an external KMS key for DNSSEC
#####

module "external" {
  source = "../../"

  current_account_id = data.aws_caller_identity.current.account_id
  current_region     = data.aws_region.current.name
  testing_prefix     = local.prefix
  vpc_id             = data.aws_vpc.default.id

  existing_zones = [
    {
      logical_name       = "private.tftest.com"
      zone_id            = aws_route53_zone.private.zone_id
      vpc_attachment_ids = [aws_vpc.main.id]
    },
    {
      logical_name = "public.tftest.com"
      zone_id      = aws_route53_zone.public.zone_id
      dnssec       = true
    },
  ]

  dnssec_enabled              = true
  dnssec_ksk_name             = "${local.prefix}ksktftest"
  dnssec_external_kms_key_arn = aws_kms_key.example.arn

  records = [
    {
      zone_logical_name = "public.tftest.com"
      name              = "record"
      logical_name      = "record"
      type              = "A"
      ttl               = 3600
      records           = ["1.2.3.4"]
    },
    {
      zone_logical_name = "private.tftest.com"
      name              = "record"
      logical_name      = "record"
      type              = "A"
      ttl               = 80
      records           = ["1.2.3.4"]
    },
  ]

  providers = {
    aws.replica = aws.replica
  }
}
