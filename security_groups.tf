#####
# Resolver security group inbound
#####

resource "aws_security_group" "this_inbound" {
  count = var.resolver_inbound_count

  name        = var.resolver_inbound_security_group_name
  description = "Security group for inbound resolvers."
  vpc_id      = data.aws_subnet.this_inbound[count.index].vpc_id

  tags = merge(
    local.tags,
    {
      Name = var.resolver_inbound_security_group_name
    },
    var.tags,
    var.resolver_tags,
  )
}

resource "aws_security_group_rule" "this_inbound_53_tcp" {
  count = length(var.resolver_inbound_security_group_ingress_allowed_cidrs) > 0 ? var.resolver_inbound_count : 0

  security_group_id = aws_security_group.this_inbound[count.index].id

  type        = "ingress"
  from_port   = 53
  to_port     = 53
  protocol    = "tcp"
  cidr_blocks = var.resolver_inbound_security_group_ingress_allowed_cidrs
}

resource "aws_security_group_rule" "this_inbound_53_udp" {
  count = length(var.resolver_inbound_security_group_ingress_allowed_cidrs) > 0 ? var.resolver_inbound_count : 0

  security_group_id = aws_security_group.this_inbound[count.index].id

  type        = "ingress"
  from_port   = 53
  to_port     = 53
  protocol    = "udp"
  cidr_blocks = var.resolver_inbound_security_group_ingress_allowed_cidrs
}

resource "aws_security_group_rule" "this_inbound_853_tcp" {
  count = length(var.resolver_inbound_security_group_ingress_allowed_cidrs) > 0 ? var.resolver_inbound_count : 0

  security_group_id = aws_security_group.this_inbound[count.index].id

  type        = "ingress"
  from_port   = 853
  to_port     = 853
  protocol    = "tcp"
  cidr_blocks = var.resolver_inbound_security_group_ingress_allowed_cidrs
}

resource "aws_security_group_rule" "this_inbound_853_udp" {
  count = length(var.resolver_inbound_security_group_ingress_allowed_cidrs) > 0 ? var.resolver_inbound_count : 0

  security_group_id = aws_security_group.this_inbound[count.index].id

  type        = "ingress"
  from_port   = 853
  to_port     = 853
  protocol    = "udp"
  cidr_blocks = var.resolver_inbound_security_group_ingress_allowed_cidrs
}

resource "aws_security_group_rule" "this_inbound_out_53_tcp" {
  count = length(var.resolver_inbound_security_group_egress_allowed_cidrs) > 0 ? var.resolver_inbound_count : 0

  security_group_id = aws_security_group.this_inbound[count.index].id

  type        = "egress"
  from_port   = 53
  to_port     = 53
  protocol    = "tcp"
  cidr_blocks = var.resolver_inbound_security_group_egress_allowed_cidrs
}

resource "aws_security_group_rule" "this_inbound_out_53_udp" {
  count = length(var.resolver_inbound_security_group_egress_allowed_cidrs) > 0 ? var.resolver_inbound_count : 0

  security_group_id = aws_security_group.this_inbound[count.index].id

  type        = "egress"
  from_port   = 53
  to_port     = 53
  protocol    = "udp"
  cidr_blocks = var.resolver_inbound_security_group_egress_allowed_cidrs
}

resource "aws_security_group_rule" "this_inbound_out_853_tcp" {
  count = length(var.resolver_inbound_security_group_egress_allowed_cidrs) > 0 ? var.resolver_inbound_count : 0

  security_group_id = aws_security_group.this_inbound[count.index].id

  type        = "egress"
  from_port   = 853
  to_port     = 853
  protocol    = "tcp"
  cidr_blocks = var.resolver_inbound_security_group_egress_allowed_cidrs
}

resource "aws_security_group_rule" "this_inbound_out_853_udp" {
  count = length(var.resolver_inbound_security_group_egress_allowed_cidrs) > 0 ? var.resolver_inbound_count : 0

  security_group_id = aws_security_group.this_inbound[count.index].id

  type        = "egress"
  from_port   = 853
  to_port     = 853
  protocol    = "udp"
  cidr_blocks = var.resolver_inbound_security_group_egress_allowed_cidrs
}

#####
# Resolver security group outbound
#####

resource "aws_security_group" "this_outbound" {
  count = var.resolver_outbound_count

  name        = var.resolver_outbound_security_group_name
  description = "Security group for outbound resolvers."
  vpc_id      = data.aws_subnet.this_outbound[count.index].vpc_id

  tags = merge(
    {
      "Terraform" = "true"
    },
    {
      "Name" = var.resolver_outbound_security_group_name
    },
    var.tags,
    var.resolver_tags,
  )
}

resource "aws_security_group_rule" "this_outbound_53_tcp" {
  count = length(var.resolver_outbound_security_group_ingress_allowed_cidrs) > 0 ? var.resolver_outbound_count : 0

  security_group_id = aws_security_group.this_outbound[count.index].id

  type        = "ingress"
  from_port   = 53
  to_port     = 53
  protocol    = "tcp"
  cidr_blocks = var.resolver_outbound_security_group_ingress_allowed_cidrs
}

resource "aws_security_group_rule" "this_outbound_53_udp" {
  count = length(var.resolver_outbound_security_group_ingress_allowed_cidrs) > 0 ? var.resolver_outbound_count : 0

  security_group_id = aws_security_group.this_outbound[count.index].id

  type        = "ingress"
  from_port   = 53
  to_port     = 53
  protocol    = "udp"
  cidr_blocks = var.resolver_outbound_security_group_ingress_allowed_cidrs
}

resource "aws_security_group_rule" "this_outbound_853_tcp" {
  count = length(var.resolver_outbound_security_group_ingress_allowed_cidrs) > 0 ? var.resolver_outbound_count : 0

  security_group_id = aws_security_group.this_outbound[count.index].id

  type        = "ingress"
  from_port   = 853
  to_port     = 853
  protocol    = "tcp"
  cidr_blocks = var.resolver_outbound_security_group_ingress_allowed_cidrs
}

resource "aws_security_group_rule" "this_outbound_853_udp" {
  count = length(var.resolver_outbound_security_group_ingress_allowed_cidrs) > 0 ? var.resolver_outbound_count : 0

  security_group_id = aws_security_group.this_outbound[count.index].id

  type        = "ingress"
  from_port   = 853
  to_port     = 853
  protocol    = "udp"
  cidr_blocks = var.resolver_outbound_security_group_ingress_allowed_cidrs
}

resource "aws_security_group_rule" "this_outbound_out_53_tcp" {
  count = length(var.resolver_outbound_security_group_egress_allowed_cidrs) > 0 ? var.resolver_outbound_count : 0

  security_group_id = aws_security_group.this_outbound[count.index].id

  type        = "egress"
  from_port   = 53
  to_port     = 53
  protocol    = "tcp"
  cidr_blocks = var.resolver_outbound_security_group_egress_allowed_cidrs
}

resource "aws_security_group_rule" "this_outbound_out_53_udp" {
  count = length(var.resolver_outbound_security_group_egress_allowed_cidrs) > 0 ? var.resolver_outbound_count : 0

  security_group_id = aws_security_group.this_outbound[count.index].id

  type        = "egress"
  from_port   = 53
  to_port     = 53
  protocol    = "udp"
  cidr_blocks = var.resolver_outbound_security_group_egress_allowed_cidrs
}

resource "aws_security_group_rule" "this_outbound_out_853_tcp" {
  count = length(var.resolver_outbound_security_group_egress_allowed_cidrs) > 0 ? var.resolver_outbound_count : 0

  security_group_id = aws_security_group.this_outbound[count.index].id

  type        = "egress"
  from_port   = 853
  to_port     = 853
  protocol    = "tcp"
  cidr_blocks = var.resolver_outbound_security_group_egress_allowed_cidrs
}

resource "aws_security_group_rule" "this_outbound_out_853_udp" {
  count = length(var.resolver_outbound_security_group_egress_allowed_cidrs) > 0 ? var.resolver_outbound_count : 0

  security_group_id = aws_security_group.this_outbound[count.index].id

  type        = "egress"
  from_port   = 853
  to_port     = 853
  protocol    = "udp"
  cidr_blocks = var.resolver_outbound_security_group_egress_allowed_cidrs
}
