locals {
  tags = merge(
    var.tags,
    {
      managed-by = "terraform"
      origin     = "gitlab.com/wild-beavers/terraform/module-aws-route53"
    },
  )
}

#####
# Hosted zone
#####

locals {
  delegation_sets = { for zone in var.public_zones : zone.logical_name => zone if zone.delegation_set == true }

  public_zones = { for zone in var.public_zones : zone.logical_name => zone }

  private_zones = { for zone in var.private_zones : zone.logical_name => zone }

  existing_zones = { for zone in var.existing_zones : zone.logical_name => zone }

  zone_vpc_attachment_ids = concat(flatten([
    for zone in var.private_zones : [
      for vpc_id in zone.vpc_attachment_ids :
      {
        zone_id = aws_route53_zone.private[zone.logical_name].id
        vpc_id  = vpc_id
      }
    ]
    ]),
    flatten([
      for zone in var.existing_zones : [
        for vpc_id in zone.vpc_attachment_ids :
        {
          zone_id = zone.zone_id
          vpc_id  = vpc_id
        }
      ]
    ])
  )
}

resource "aws_route53_delegation_set" "this" {
  for_each = local.delegation_sets

  reference_name = each.key
}

resource "aws_route53_zone" "public" {
  for_each = local.public_zones

  name              = format("%s%s%s", var.testing_prefix, each.value.prefix, each.value.name)
  comment           = each.value.comment
  delegation_set_id = each.value.delegation_set == true ? aws_route53_delegation_set.this[each.key].id : null

  tags = merge(
    local.tags,
    {
      documentation-link = "https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone"
      Name               = each.value.name
      Description        = each.value.comment
    },
    each.value.tags,
  )
}

resource "aws_route53_zone" "private" {
  for_each = local.private_zones

  name    = format("%s%s%s", var.testing_prefix, each.value.prefix, each.value.name)
  comment = each.value.comment

  vpc {
    vpc_id = var.vpc_id
  }

  tags = merge(
    local.tags,
    {
      documentation-link = "https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone"
      Name               = each.key
      Description        = each.value.comment
    },
    each.value.tags,
  )

  lifecycle {
    ignore_changes = [vpc]
  }
}

resource "aws_route53_zone_association" "this" {
  count = length(local.zone_vpc_attachment_ids)

  zone_id = element(local.zone_vpc_attachment_ids, count.index)["zone_id"]
  vpc_id  = element(local.zone_vpc_attachment_ids, count.index)["vpc_id"]
}

#####
# Records
#####

locals {
  records = { for record in var.records : format("%s_%s_%s", record.logical_name, record.zone_logical_name, record.type) => record }

  alias_records = { for record in var.alias_records : format("%s_%s_%s", record.logical_name, record.zone_logical_name, record.type) => record }

  all_zones_ids = merge(
    {
      for zone in var.private_zones : zone.logical_name => aws_route53_zone.private[zone.logical_name].zone_id
    },
    {
      for zone in var.public_zones : zone.logical_name => aws_route53_zone.public[zone.logical_name].zone_id
    },
    {
      for zone in var.existing_zones : zone.logical_name => zone.zone_id
    },
  )
}

resource "aws_route53_record" "this" {
  for_each = local.records

  zone_id         = local.all_zones_ids[each.value.zone_logical_name]
  name            = format("%s%s", var.testing_prefix, each.value.name)
  type            = each.value.type
  ttl             = each.value.ttl
  records         = each.value.records
  allow_overwrite = true
}

resource "aws_route53_record" "this_alias" {
  for_each = local.alias_records

  zone_id         = local.all_zones_ids[each.value.zone_logical_name]
  name            = format("%s%s", var.testing_prefix, each.value.name)
  type            = each.value.type
  allow_overwrite = true

  alias {
    name                   = each.value.alias_name
    zone_id                = each.value.alias_zone_id
    evaluate_target_health = each.value.alias_evaluate_target_health
  }
}
